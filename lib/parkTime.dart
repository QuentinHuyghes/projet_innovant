import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:projet_innovant/main.dart';
import 'package:intl/intl.dart';

class ParkTimeContent extends StatefulWidget {
  int time;

  ParkTimeContent({Key key, @required this.time}) : super(key: key);

  @override
  _Res createState() => _Res(this.time);
}

class _Res extends State<ParkTimeContent> {
  int time;

  _Res(this.time);

  static final DateTime now = DateTime.now();
  static final DateFormat formatHour = new DateFormat.Hm();

  String timeEnd = "";
  String countdown = "";

  var duration;

  void setDurationAndDate() {
    setState(() {
      if (time <= 8) {
        duration = Duration(hours: time);
      } else {
        duration = Duration(minutes: time);
      }
      DateTime res = now.add(duration);
      timeEnd = formatHour.format(res);
    });
  }

  void timer() {
    if (time <= 8) {
      Timer.periodic(Duration(seconds: 1), (timer) {
        var d = Duration(seconds: 1);
        setState(() {
          countdown = "";
          duration -= d;
          List<String> parts = duration.toString().split(':');
          if (int.parse(parts[0]) > 0) {
            countdown = parts[0] + ' heure ';
          }
          countdown +=
              parts[1] + ' minutes ' + parts[2].split('.')[0] + ' secondes';
        });
      });
    } else {
      Timer.periodic(Duration(seconds: 1), (timer) {
        var d = Duration(seconds: 1);
        setState(() {
          duration -= d;
          List<String> parts = duration.toString().split(':');
          countdown = parts[1] + ':' + parts[2].split('.')[0];
        });
      });
    }
  }

  @override
  void initState() {
    super.initState();
    setDurationAndDate();
    timer();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: IntrinsicWidth(
            child: Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 50, bottom: 50),
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.black,
                width: 1,
              ),
              borderRadius: BorderRadius.circular(5),
            ),
            child: Padding(
              padding:
                  EdgeInsets.only(left: 10, right: 10, top: 50, bottom: 50),
              child: Column(
                children: <Widget>[
                  Center(
                    child:
                        Text("Temps restant", style: TextStyle(fontSize: 20)),
                  ),
                  Center(
                    child: Text(countdown, style: TextStyle(fontSize: 20)),
                  ),
                  Center(
                    child:
                        Text("Fin du temps à", style: TextStyle(fontSize: 20)),
                  ),
                  Center(child: Text(timeEnd, style: TextStyle(fontSize: 20)))
                ],
              ),
            ),
          ),
        ),
        ButtonTheme(
            child: RaisedButton(
                child: Text(
                  "Ajouter du temps",
                  style: TextStyle(fontSize: 20, color: Colors.white),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ChoseTime()),
                  );
                })),
        ButtonTheme(
            child: RaisedButton(
                child: Text(
                  "Je quitte ma place",
                  style: TextStyle(fontSize: 20, color: Colors.white),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Homepage()),
                  );
                }))
      ],
    )));
  }
}
