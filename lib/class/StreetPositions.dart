import 'dart:convert';
import 'dart:math';

import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;

class StreetMarker {
  Geolocator geolocator = Geolocator();

  Position southWest, northEast, center, begin, end;
  List<Position> positions, places;
  double streetLength;

  String apiKey = "AIzaSyDtPBKVnxlgBaJYYdwA8W624FkBJhnQ1r8";

  void getLocationFromAddress(address) async {
    String url =
        "https://maps.googleapis.com/maps/api/geocode/json?address=$address&key=$apiKey";
    http.Response response = await http.get(url);

    Map res = jsonDecode(response.body);
    var bounds = res["results"][0]["geometry"]["bounds"];
    var location = res["results"][0]["geometry"]["location"];

    var lng = bounds["southwest"]["lng"];
    var lat = bounds["southwest"]["lat"];

    southWest = new Position(longitude: lng, latitude: lat);

    lng = bounds["northeast"]["lng"];
    lat = bounds["northeast"]["lat"];

    northEast = new Position(longitude: lng, latitude: lat);

    lng = location["lng"];
    lat = location["lat"];

    center = new Position(longitude: lng, latitude: lat);
  }

  void setBeginEnd() async {
    Position northWest, southEast;

    northWest = new Position(
        latitude: northEast.latitude, longitude: southWest.longitude);
    southEast = new Position(
        latitude: southWest.latitude, longitude: northEast.longitude);

    double l1, l2;

    await getDistanceBetween(northWest, northEast).then((distanceMeter) {
      l1 = distanceMeter;
    });
    await getDistanceBetween(northWest, southWest).then((distanceMeter) {
      l2 = distanceMeter;
    });

    if (l1 < l2) {
      var lng = (northWest.longitude + northEast.longitude) / 2;
      begin = new Position(latitude: northWest.latitude, longitude: lng);
      lng = (southWest.longitude + southEast.longitude) / 2;
      end = new Position(latitude: southWest.latitude, longitude: lng);
    } else {
      var lat = (northWest.latitude + southWest.latitude) / 2;
      begin = new Position(latitude: lat, longitude: northWest.longitude);
      lat = (northEast.latitude + southEast.latitude) / 2;
      end = new Position(latitude: lat, longitude: northEast.longitude);
    }
  }

  void getStartEndRoad(Position begin, Position end, Position center) async {
    String url = "https://maps.googleapis.com/maps/api/directions/json?"
            "origin=" +
        begin.latitude.toString() +
        "," +
        begin.longitude.toString() +
        "&destination=" +
        center.latitude.toString() +
        "," +
        center.longitude.toString() +
        "&key=$apiKey";
    http.Response response = await http.get(url);

    Map res = jsonDecode(response.body);

    var a = res["routes"][0]["legs"][0]["steps"][0]["end_location"];
    this.begin = new Position(latitude: a["lat"], longitude: a["lng"]);

    url = "https://maps.googleapis.com/maps/api/directions/json?"
            "origin=" +
        center.latitude.toString() +
        "," +
        center.longitude.toString() +
        "&destination=" +
        end.latitude.toString() +
        "," +
        end.longitude.toString() +
        "&key=$apiKey";
    response = await http.get(url);

    res = jsonDecode(response.body);

    a = res["routes"][0]["legs"][0]["steps"][0]["end_location"];
    this.end = new Position(latitude: a["lat"], longitude: a["lng"]);
  }

  void getAllPointsOfStreet(
      Position begin, Position end, Position center) async {
    String url = "https://roads.googleapis.com/v1/snapToRoads?"
            "path=" +
        begin.latitude.toString() +
        "," +
        begin.longitude.toString() +
        "|" +
        center.latitude.toString() +
        "," +
        center.longitude.toString() +
        "|" +
        end.latitude.toString() +
        "," +
        end.longitude.toString() +
        "&interpolate=true&key=$apiKey";
    http.Response response = await http.get(url);

    Map res = jsonDecode(response.body);

    var res2 = res["snappedPoints"];

    positions = new List<Position>();

    positions.add(new Position(
        latitude: res2[0]["location"]["latitude"],
        longitude: res2[0]["location"]["longitude"]));

    for (int i = 1; i < res2.length; i++) {
      if ((res2[i]["location"]["latitude"] !=
              res2[i - 1]["location"]["latitude"]) &&
          (res2[i]["location"]["longitude"] !=
              res2[i - 1]["location"]["longitude"])) {
        Position p = new Position(
            latitude: res2[i]["location"]["latitude"],
            longitude: res2[i]["location"]["longitude"]);
        positions.add(p);
      }
    }
  }

  void calculStreetLength() async {
    streetLength = 0.0;
    for (int i = 0; i < positions.length; i++) {
      if (i != positions.length - 1) {
        Position p1 = new Position(
            latitude: positions[i].latitude, longitude: positions[i].longitude);
        Position p2 = new Position(
            latitude: positions[i + 1].latitude,
            longitude: positions[i + 1].longitude);
        await geolocator
            .distanceBetween(
                p1.latitude, p1.longitude, p2.latitude, p2.longitude)
            .then((onValue) {
          streetLength += onValue;
        });
      }
    }
  }

  circleCircleIntersectionPoints(
      Position c1, Position c2, double r1, double r2) {
    var r, R, d, dx, dy, cx, cy, Cx, Cy;

    var EPS = 0.0000001;

    if (r1 < r2) {
      r = r1;
      R = r2;
      cx = c1.latitude;
      cy = c1.longitude;
      Cx = c2.latitude;
      Cy = c2.longitude;
    } else {
      r = r2;
      R = r1;
      Cx = c1.latitude;
      Cy = c1.longitude;
      cx = c2.latitude;
      cy = c2.longitude;
    }

    // Compute the vector <dx, dy>
    dx = cx - Cx;
    dy = cy - Cy;

    // Find the distance between two points.
    d = sqrt(dx * dx + dy * dy);

    // There are an infinite number of solutions
    // Seems appropriate to also return null
    if (d < EPS && (R - r).abs() < EPS)
      return [];

    // No intersection (circles centered at the
    // same place with different size)
    else if (d < EPS) return [];

    var x = (dx / d) * R + Cx;
    var y = (dy / d) * R + Cy;
    var P = new Position(latitude: x, longitude: y);

    // Single intersection (kissing circles)
    if (((R + r) - d).abs() < EPS || (R - (r + d)).abs() < EPS) return [P];

    // No intersection. Either the small circle contained within
    // big circle or circles are simply disjoint.
    if ((d + r) < R || (R + r < d)) return [];

    var C = new Position(latitude: Cx, longitude: Cy);
    var angle = acossafe((r * r - d * d - R * R) / (-2.0 * d * R));
    var pt1 = rotatePoint(C, P, angle);
    var pt2 = rotatePoint(C, P, -angle);
    return [pt1, pt2];
  }

  acossafe(x) {
    if (x >= 1.0) return 0;
    if (x <= -1.0) return pi;
    return acos(x);
  }

  rotatePoint(fp, pt, a) {
    var x = pt.x - fp.x;
    var y = pt.y - fp.y;
    var xRot = x * cos(a) + y * sin(a);
    var yRot = y * cos(a) - x * sin(a);
    return new Position(latitude: fp.x + xRot, longitude: fp.y + yRot);
  }

  void setPlaces() async {
    places = new List<Position>();
    var tab;
    var nb = 0.0;

    for (int i = 0; i < positions.length; i++) {
      if (i != positions.length - 1) {
        Position p1 = new Position(
            latitude: positions[i].latitude, longitude: positions[i].longitude);
        Position p2 = new Position(
            latitude: positions[i + 1].latitude,
            longitude: positions[i + 1].longitude);

        await getDistanceBetween(p1, p2).then((distanceMeter) {
          nb = distanceMeter / 10;
        });

        for (int j = 0; j < nb.floor(); j++) {
          double dist = sqrt(pow(p2.longitude - p1.longitude, 2) +
              pow(p2.latitude - p1.latitude, 2));

          double r1 = dist / (nb - j);
          double r2 = dist - r1;
          tab = circleCircleIntersectionPoints(p1, p2, r1, r2);

          p1 = new Position(
              latitude: tab[0].latitude, longitude: tab[0].longitude);
          places.add(p1);
        }
      }
    }
  }

  Future<double> getDistanceBetween(Position p1, Position p2) async {
    double res;
    await geolocator
        .distanceBetween(p1.latitude, p1.longitude, p2.latitude, p2.longitude)
        .then((onValue) {
      res = onValue;
    });
    return res;
  }

  void create() async {
    await getLocationFromAddress("rue royale 62100 calais");

    await setBeginEnd();
    var x = begin;
    begin = end;
    end = x;
    await getStartEndRoad(begin, end, center);
    await getAllPointsOfStreet(begin, end, center);
    await calculStreetLength();
    await setPlaces();
  }
}
