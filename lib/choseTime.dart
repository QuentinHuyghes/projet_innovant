import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'main.dart';

class ChoseTimeContent extends StatefulWidget {
  ChoseTimeContent({Key key}) : super(key: key);
  @override
  _TimePark createState() => _TimePark();
}

class _TimePark extends State<ChoseTimeContent> {
  final _listTimes = [15, 30, 45, 1, 2, 3, 4, 5, 6, 7, 8];
  final _listPrices = [15.0, 50.0, 1.0, 1.10, 1.5, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0];

  double _price = 15;
  double _sliderValue = 0;

  int _time = 15;

  @override
  Widget build(BuildContext context) {
    return new ListView(
      children: <Widget>[
        Center(
            child: Padding(
          child: Text("Choix rapide", style: TextStyle(fontSize: 20)),
          padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
        )),
        Center(
            child: IntrinsicWidth(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
              ButtonTheme(
                  child: RaisedButton(
                      child: Text(
                        "30 minutes (50 cts)",
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        setState(() {
                          _time = 30;
                          _price = 50;
                        });
                      })),
              ButtonTheme(
                  child: RaisedButton(
                      child: Text(
                        "1 heure (1 euro)",
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        setState(() {
                          _time = 1;
                          _price = 1;
                        });
                      })),
              ButtonTheme(
                  child: RaisedButton(
                      child: Text(
                        "2 heure (2 euros)",
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        setState(() {
                          _time = 2;
                          _price = 2;
                        });
                      })),
              ButtonTheme(
                  child: RaisedButton(
                      child: Text(
                        "3 heure (2.5 euros)",
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        setState(() {
                          _time = 3;
                          _price = 2.5;
                        });
                      }))
            ]))),
        Center(
            child: Padding(
          child: Text("Choix avancé", style: TextStyle(fontSize: 20)),
          padding: EdgeInsets.only(top: 10.0),
        )),
        Slider(
          min: 0,
          max: 10,
          divisions: 10,
          value: _sliderValue,
          label: _listTimes.elementAt(_sliderValue.toInt()) <= 8
              ? _listTimes.elementAt(_sliderValue.toInt()).toString() + "h"
              : _listTimes.elementAt(_sliderValue.toInt()).toString() + "min",
          onChanged: (value) {
            setState(() {
              _time = _listTimes.elementAt(_sliderValue.toInt());
              _price = _listPrices.elementAt(_sliderValue.toInt());
              _sliderValue = value;
            });
          },
        ),
        Padding(
            padding:
                EdgeInsets.only(left: 50.0, right: 50.0, top: 10, bottom: 10),
            child: Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.black,
                    width: 1,
                  ),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Padding(
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    child: Column(children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(bottom: 5),
                        child:
                            Text("Votre choix", style: TextStyle(fontSize: 15)),
                      ),
                      Text(_time <= 8
                          ? _time.toString() + " heure"
                          : _time.toString() + " minutes"),
                      Text(_price <= 7.0
                          ? _price.toString() + " euros"
                          : _price.toString() + " cts")
                    ])))),
        Padding(
          padding: EdgeInsets.only(left: 10, right: 10),
          child: TextFormField(
              decoration: InputDecoration(labelText: "Immatriculation")),
        ),
        Center(
            child: ButtonTheme(
                child: RaisedButton(
                    child: Text("Reserver place",
                        style: TextStyle(color: Colors.white)),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ParkTime(time: _time)),
                      );
                    })))
      ],
    );
  }
}
