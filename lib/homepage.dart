import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:convert';
import 'dart:async';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'main.dart';
import 'class/StreetPositions.dart';

class HomePageContent extends StatefulWidget {
  HomePageContent({Key key}) : super(key: key);

  @override
  _HomePageContentState createState() => _HomePageContentState();
}

class _HomePageContentState extends State<HomePageContent> {
  final String apiKey = "AIzaSyDtPBKVnxlgBaJYYdwA8W624FkBJhnQ1r8";

  final Geolocator geolocator = Geolocator();
  final StreetMarker streetMarker = new StreetMarker();
  final Completer<GoogleMapController> _controller = Completer();

  final _formKey = GlobalKey<FormState>();
  final addressController = TextEditingController();

  final List<Marker> markers = <Marker>[];

  Future<Position> _future;

  LatLng SOURCE_LOCATION;
  LatLng DEST_LOCATION;

  Set<Polyline> _polylines = {};
  List<LatLng> polylineCoordinates = [];
  final PolylinePoints polylinePoints = PolylinePoints();

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  void getPlacesMarkers(String ville, String rue) async {
    markers.clear();

    String json =
        await DefaultAssetBundle.of(context).loadString("assets/places.json");

    List<dynamic> map = jsonDecode(json);
    List<dynamic> villes = map[0]["villes"];

    for (int i = 0; i < villes.length; i++) {
      if (villes[i]["nom"] == ville) {
        List<dynamic> rues = villes[i]["rues"];
        for (int j = 0; j < rues.length; j++) {
          if (rues[j]["nom"] == rue) {
            List<dynamic> positions = rues[j]["positions"];
            for (int k = 0; k < positions.length; k++) {
              if (positions[k]["places"] > 0) {
                markers.add(Marker(
                  markerId: MarkerId(positions[k]["Lat"].toString()),
                  position: LatLng(positions[k]["Lat"], positions[k]["Long"]),
                ));
              }
            }
          }
        }
      }
    }
  }

  Future<Position> updateLocation(String ville, String rue) async {
    Position newPosition;
    if (rue == "" && ville == "") {
      newPosition = await geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      setState(() {
        SOURCE_LOCATION =
            new LatLng(newPosition.latitude, newPosition.longitude);
      });
    } else {
      await getPlacesMarkers(ville, rue);
      setState(() {
        DEST_LOCATION = markers.elementAt(0).position;
      });
      newPosition = new Position(
          latitude: SOURCE_LOCATION.latitude,
          longitude: SOURCE_LOCATION.longitude);
      setPolylines();
    }

    return newPosition;
  }

  void setPolylines() async {
    _polylines.clear();
    polylineCoordinates.clear();

    List<PointLatLng> result = await polylinePoints?.getRouteBetweenCoordinates(
        apiKey,
        SOURCE_LOCATION.latitude,
        SOURCE_LOCATION.longitude,
        DEST_LOCATION.latitude,
        DEST_LOCATION.longitude);

    if (result.isNotEmpty) {
      result.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }
    setState(() {
      Polyline polyline = Polyline(
          polylineId: PolylineId("poly"),
          color: Color.fromARGB(255, 40, 122, 198),
          points: polylineCoordinates);

      _polylines.add(polyline);
    });
  }

  @override
  void initState() {
    super.initState();
    _future = updateLocation("", "");
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
        child: ListView(physics: NeverScrollableScrollPhysics(), children: <
            Widget>[
      Padding(
        padding: EdgeInsets.all(8),
        child: Form(
            key: _formKey,
            child: Column(children: <Widget>[
              TextFormField(
                  controller: addressController,
                  decoration: InputDecoration(labelText: "Adresse")),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ButtonTheme(
                    child: RaisedButton(
                        child: Text(
                          "Rechercher",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            setState(() {
                              _future = updateLocation(
                                  "calais", addressController.text.toString());
                            });
                          }
                        }),
                  ),
                  ButtonTheme(
                    child: RaisedButton(
                        child: Text(
                          "Reserver place",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => ChoseTime()),
                          );
                        }),
                  )
                ],
              )
            ])),
      ),
      FutureBuilder(
          future: _future,
          // ignore: missing_return
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (!snapshot.hasError) {
                return new Container(
                    height: 440,
                    child: new GoogleMap(
                      mapType: MapType.normal,
                      onMapCreated: _onMapCreated,
                      initialCameraPosition: CameraPosition(
                          target: LatLng(
                              snapshot.data.latitude, snapshot.data.longitude),
                          zoom: 15),
                      markers: Set<Marker>.of(markers),
                      myLocationEnabled: true,
                      polylines: _polylines,
                    ));
              }
            } else {
              return Center(child: CircularProgressIndicator());
            }
          }),
    ]));
  }
}
