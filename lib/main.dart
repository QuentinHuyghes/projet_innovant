import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:projet_innovant/parkTime.dart';

import 'homepage.dart';
import 'choseTime.dart';

void main() => runApp(Homepage());

var title = 'NoStressPark';

class Homepage extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            title: Text(title),
            leading: Image.asset('assets/logo/logo_projet.png', fit: BoxFit.scaleDown,),
          ),
          body: HomePageContent()),
      debugShowCheckedModeBanner: false,
    );
  }
}

class ChoseTime extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            title: Text(title),
              leading: Image.asset('assets/logo/logo_projet.png')
          ),
          body: ChoseTimeContent()),
      debugShowCheckedModeBanner: false,
    );
  }
}

class ParkTime extends StatelessWidget {
  final int time;
  ParkTime({Key key, @required this.time}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            title: Text(title),
              leading: Image.asset('assets/logo/logo_projet.png')
          ),
          body: ParkTimeContent(time: time)),
      debugShowCheckedModeBanner: false,
    );
  }
}